﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;
using System.Web.Http.Cors;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/LMS")]
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class LMSController : ApiController
    {
        private DBModel db = new DBModel();
        private object logger;

        // GET: api/LMS
        //public IQueryable<Leave> GetLeaves()
        //{
        //    return db.Leaves;
        //}

       

        [AllowAnonymous]
        [Route("GetLeavesByEmployeeID")]
        [HttpGet]
        public async Task<IHttpActionResult> GetLeavesByEmployeeID(int EmployeeID)
        {
            try
            {
                //logger.Info("Get Employee by ID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.LMSManager().getLeavebyID(EmployeeID));
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        [AllowAnonymous]
        [Route("GetAllLeaves")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllLeaves()
        {
            try
            {
                //logger.Info("Get Employee by ID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.LMSManager().getAllLeaves());
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("UpsertLeave")]
        [HttpPost]
        public IHttpActionResult UpsertLeave([FromBody] Leave Leave)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.LMSManager().upsertLeave(Leave));
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        [AllowAnonymous]
        [Route("DeleteLeave")]
        [HttpDelete]
        public IHttpActionResult DeleteLeave(int LeaveID)
        {
            try
            {
                //logger.Info("Delete QuestionAnswer by DeleteQuestionAnswerID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                return Ok(new BL.LMSManager().deleteLeave(LeaveID));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("ApproveLeave")]
        [HttpDelete]
        public IHttpActionResult ApproveLeave(int LeaveID)
        {
            try
            {
                //logger.Info("Delete QuestionAnswer by DeleteQuestionAnswerID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                return Ok(new BL.LMSManager().approveLeave(LeaveID));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("DeclineLeave")]
        [HttpDelete]
        public IHttpActionResult DeclineLeave(int LeaveID)
        {
            try
            {
                //logger.Info("Delete QuestionAnswer by DeleteQuestionAnswerID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                return Ok(new BL.LMSManager().declineLeave(LeaveID));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("GetType")]
        [HttpGet]
        public async Task<IHttpActionResult> GetType()
        {
            try
            {
                //logger.Info("Get Employee by ID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.LMSManager().getType());
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("GetLeaveByXrefID")]
        [HttpGet]
        public async Task<IHttpActionResult> GetLeaveByXrefID(int LeaveXREFID)
        {
            try
            {
                //logger.Info("Get Employee by ID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.LMSManager().getLeaveByXrefID(LeaveXREFID));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("GetLeavesXref")]
        [HttpGet]
        public async Task<IHttpActionResult> GetLeavesXref(int EmployeeID)
        {
            try
            {
                //logger.Info("Get Employee by ID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.LMSManager().getLeavesXref(EmployeeID));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("UpsertLeaveXref")]
        [HttpPost]
        public IHttpActionResult UpsertLeaveXref([FromBody] LeaveXREF LeaveXREF)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.LMSManager().upsertLeaveXref(LeaveXREF));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("GetAttendance")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAttendance(int AttendanceID)
        {
            try
            {
                //logger.Info("Get Employee by ID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.LMSManager().getAttendance(AttendanceID));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("GetAttendanceByEmp")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAttendanceByEmp(int EmployeeID, int WeekID)
        {
            try
            {
                //logger.Info("Get Employee by ID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.LMSManager().getAttendanceByEmp(EmployeeID, WeekID));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("UpsertAttendance")]
        [HttpPost]
        public IHttpActionResult UpsertAttendance([FromBody] Attendance Attendance)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.LMSManager().upsertAttendance(Attendance));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("GetAttendanceXref")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAttendanceXref(int AttendanceXREFID)
        {
            try
            {
                //logger.Info("Get Employee by ID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.LMSManager().getAttendanceXref(AttendanceXREFID));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("GetAttendanceXrefs")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAttendanceXrefs(int AttendanceXREFID, int WeekID)
        {
            try
            {
                //logger.Info("Get Employee by ID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.LMSManager().getAttendanceXrefs(AttendanceXREFID,  WeekID));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("GetAttendanceXrefByS")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAttendanceXrefByS(int EmployeeID, int AttendanceStatusID)
        {
            try
            {
                //logger.Info("Get Employee by ID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.LMSManager().getAttendanceXrefByS(EmployeeID, AttendanceStatusID));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("UpsertAttendanceXref")]
        [HttpPost]
        public IHttpActionResult UpsertAttendanceXref([FromBody] AttendanceXREF AttendanceXREF)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.LMSManager().upsertAttendanceXref(AttendanceXREF));
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        //[AllowAnonymous]
        //[Route("GetType")]
        //[HttpGet]
        //public async Task<IHttpActionResult> GetType()
        //{
        //    try
        //    {
        //        //logger.Info("Get Employee by ID Called");
        //        if (!ModelState.IsValid)
        //        {
        //            return BadRequest(ModelState);
        //        }
        //        return Ok(new BL.LMSManager().getType());
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}

        //// GET: api/LMS/5
        //[ResponseType(typeof(Leave))]
        //public IHttpActionResult GetLeave(int id)
        //{
        //    Leave leave = db.Leaves.Find(id);
        //    if (leave == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(leave);
        //}

        //// PUT: api/LMS/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutLeave(int id, Leave leave)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != leave.LeaveID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(leave).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!LeaveExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST: api/LMS
        //[ResponseType(typeof(Leave))]
        //public IHttpActionResult PostLeave(Leave leave)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Leaves.Add(leave);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = leave.LeaveID }, leave);
        //}

        //// DELETE: api/LMS/5
        //[ResponseType(typeof(Leave))]
        //public IHttpActionResult DeleteLeave(int id)
        //{
        //    Leave leave = db.Leaves.Find(id);
        //    if (leave == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Leaves.Remove(leave);
        //    db.SaveChanges();

        //    return Ok(leave);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LeaveExists(int id)
        {
            return db.Leaves.Count(e => e.LeaveID == id) > 0;
        }
    }
}