﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;
using System.Web.Http.Cors;


namespace WebAPI.Controllers
{
    [RoutePrefix("api/Employees")]
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class EmployeesController : ApiController
    {
        private DBModel db = new DBModel();
        private object logger;

        [AllowAnonymous]
        [Route("GetEmployee")]
        [HttpGet]
        public async Task<IHttpActionResult> GetEmployee(int EmployeeID)
        {
            try
            {
                //logger.Info("Get Employee by ID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.EmployeeManager().getEmployee(EmployeeID));
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        [AllowAnonymous]
        [Route("GetEmployees")]
        [HttpGet]
        public async Task<IHttpActionResult> GetEmployees()
        {
            try
            {
                //logger.Info("Get Employee by ID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.EmployeeManager().getEmployees());
            }
            catch (Exception e)
            {
                throw e;
            }
        }




        [AllowAnonymous]
        [Route("Login")]
        [HttpGet]
        public async Task<int> Login()
        {
            return 1;
        }

        [AllowAnonymous]
        [Route("Login")]
        [HttpGet]
        public async Task<IHttpActionResult> Login(string Email,string Password)
        {
            try
            {
                //logger.Info("Get Employee by ID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.EmployeeManager().getEmployeeID (Email, Password));
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        [AllowAnonymous]
        [Route("DeleteEmployee")]
        [HttpDelete]
        public IHttpActionResult DeleteEmployee(int EmployeeID)
        {
            try
            { 
                //logger.Info("Delete QuestionAnswer by DeleteQuestionAnswerID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                
                return Ok(new BL.EmployeeManager().deleteEmployee(EmployeeID));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("UpsertEmployee")]
        [HttpPost]
        public IHttpActionResult UpsertEmployee([FromBody] Employee employee)
        {
            try
            {                                    
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.EmployeeManager().upsertEmployee(employee));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("GetRoles")]
        [HttpGet]
        public async Task<IHttpActionResult> GetRoles()
        {
            try
            {
                //logger.Info("Get Employee by ID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.EmployeeManager().getRoles());
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("GetDesignations")]
        [HttpGet]
        public async Task<IHttpActionResult> GetDesignations()
        {
            try
            {
                //logger.Info("Get Employee by ID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.EmployeeManager().getDesignations());
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [Route("GetGenders")]
        [HttpGet]
        public async Task<IHttpActionResult> GetGenders()
        {
            try
            {
                //logger.Info("Get Employee by ID Called");
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(new BL.EmployeeManager().getGenders());
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmployeeExists(int id)
        {
            return db.Employees.Count(e => e.EmployeeID == id) > 0;
        }
    }
}