﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAPI.Models;

namespace WebAPI.BL
{
    public class EmployeeManager
    {
        public Employee getEmployee(int EmployeeID)
        {
            var employee = new Employee();
            using (var db = new DBModel())
            {
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    employee = db.Employees.FirstOrDefault(a => a.EmployeeID == EmployeeID);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return employee;
        }

        public List<Employee> getEmployees()
        {
            var employee = new List<Employee>();
            using (var db = new DBModel())
            {
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    employee = db.Employees.ToList();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return employee;
        }


        public int getEmployeeID(string Email, string Password)
        {
            var employee = new Employee();
            using (var db = new DBModel())
            {
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    employee = db.Employees.Single(d => d.Email == Email && d.Password == Password);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return employee.EmployeeID;
        }

        public int deleteEmployee(int EmployeeID)
        {
            using (var db = new DBModel())
            {
                try
                {
                    var deleteEmployee = db.Employees.FirstOrDefault(d => d.EmployeeID == EmployeeID);
                    db.Employees.Remove(deleteEmployee);
                    db.SaveChanges();
                    return EmployeeID;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

        }

        public int upsertEmployee(Employee employee)
        {
            using (var db = new DBModel())
            {
                if (employee.EmployeeID > 0)
                {
                    Employee e = db.Employees.Single(a => a.EmployeeID == employee.EmployeeID);
                    e.EmployeeID = employee.EmployeeID;
                    e.FirstName = employee.FirstName;
                    e.LastName = employee.LastName;
                    e.Email = employee.Email;
                    e.Password = employee.Password;
                    e.PhoneNumber = employee.PhoneNumber;
                    e.RoleID = employee.RoleID;
                    e.DesignationID = employee.DesignationID;
                    e.DateOfBirth = employee.DateOfBirth;
                    e.Address = employee.Address;
                    e.City = employee.City;
                    e.State = employee.State;
                    e.Country = employee.Country;
                    e.Zip = employee.Zip;
                    e.ProfileImage = employee.ProfileImage;
                    e.GenderID = employee.GenderID;
                }
                else
                {
                    if (employee == null) return 0;
                    //    employee.CreateDate = DateTime.Now;
                    db.Employees.Add(employee);
                }
                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    throw e;
                }
                return employee.EmployeeID;
            }
        }

        public List<Role> getRoles()
        {
            var role = new List<Role>();
            using (var db = new DBModel())
            {
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    role = db.Roles.ToList();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return role;
        }

        public List<Designation> getDesignations()
        {
            var desig = new List<Designation>();
            using (var db = new DBModel())
            {
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    desig = db.Designations.ToList();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return desig;
        }

        public List<Gender> getGenders()
        {
            var gender = new List<Gender>();
            using (var db = new DBModel())
            {
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    gender = db.Genders.ToList();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return gender;
        }
    }
}