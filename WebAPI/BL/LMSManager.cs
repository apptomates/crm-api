﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAPI.Models;

namespace WebAPI.BL
{
    public class LMSManager
    {
        private object db;

        public List<vwLeave> getLeavebyID(int EmployeeID)
        {
            var Leaves = new List<vwLeave>();
            using (var db = new DBModel())
            {
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    Leaves = db.vwLeaves.Where(a => a.EmployeeID == EmployeeID).ToList();
                    return Leaves;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }


        }

        public List<vwLeave> getAllLeaves()
        {
            var Leave = new List<vwLeave>();
            using (var db = new DBModel())
            {
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    Leave = db.vwLeaves.ToList();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return Leave;
        }

        public int upsertLeave(Leave Leave)
        {
            using (var db = new DBModel())
            {
                if (Leave.LeaveID > 0)
                {
                    Leave l = db.Leaves.Single(a => a.LeaveID == Leave.LeaveID);
                    l.LeaveID = Leave.LeaveID;
                    l.Reason = Leave.Reason;
                    l.FromDate = Leave.FromDate;
                    l.ToDate = Leave.ToDate;
                    l.LeaveTypeID = Leave.LeaveTypeID;
                    l.StatusID = l.StatusID;
                    l.EmployeeID = Leave.EmployeeID;
                }
                else
                {
                    if (Leave == null) return 0;
                    //    employee.CreateDate = DateTime.Now;
                    Leave.StatusID = 2;
                    Leave.EmployeeID = 1013;
                    db.Leaves.Add(Leave);
                }
                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    throw e;
                }
                return Leave.LeaveID;
            }
        }

        public int deleteLeave(int LeaveID)
        {
            using (var db = new DBModel())
            {
                //{
                //    Leave l = db.Leaves.Single(a => a.LeaveID == Leave.StatusID);
                //    l.LeaveID = Leave.StatusID;
                //}
                try
                {
                    var deleteLeave = db.Leaves.FirstOrDefault(d => d.LeaveID == LeaveID);
                    //db.Leaves.Remove(deleteLeave);
                    db.SaveChanges();
                    return LeaveID;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

        }

        public int approveLeave(int LeaveID)
        {
            using (var db = new DBModel())
            {
                Leave l = db.Leaves.Single(a => a.LeaveID == LeaveID);
                l.StatusID = 1;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    throw e;
                }
                return l.LeaveID;
            }
        }

        public int declineLeave(int LeaveID)
        {
            using (var db = new DBModel())
            {
                Leave l = db.Leaves.Single(a => a.LeaveID == LeaveID);
                l.StatusID = 3;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    throw e;
                }
                return l.LeaveID;
            }
        }

        public List<LeaveType> getType()
        {
            var type = new List<LeaveType>();
            using (var db = new DBModel())
            {
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    type = db.LeaveTypes.ToList();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return type;
        }

        public List<LeaveXREF> getLeaveByXrefID(int LeaveXREFID)
        {
            var Leaves = new List<LeaveXREF>();
            using (var db = new DBModel())
            {
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    Leaves = db.LeaveXREFs.Where(a => a.LeaveXREFID == LeaveXREFID).ToList();
                    return Leaves;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }


        }

        public List<LeaveXREF> getLeavesXref(int EmployeeID)
        {
            var Leaves = new List<LeaveXREF>();
            using (var db = new DBModel())
            {
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    Leaves = db.LeaveXREFs.Where(a => a.EmployeeID == EmployeeID).ToList();
                    return Leaves;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public int upsertLeaveXref(LeaveXREF LeaveXREF)
        {
            using (var db = new DBModel())
            {
                if (LeaveXREF.LeaveXREFID > 0)
                {
                    LeaveXREF l = db.LeaveXREFs.Single(a => a.LeaveXREFID == LeaveXREF.LeaveXREFID);
                    l.EmployeeID = LeaveXREF.EmployeeID;
                    l.LeaveTypeID = LeaveXREF.LeaveTypeID;
                    l.Count = LeaveXREF.Count;                   
                }
                else
                {
                    if (LeaveXREF == null) return 0;
                    db.LeaveXREFs.Add(LeaveXREF);
                }
                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    throw e;
                }
                return LeaveXREF.LeaveXREFID;
            }
        }

        public List<Attendance> getAttendance(int AttendanceID)
        {
            var Leaves = new List<Attendance>();
            using (var db = new DBModel())
            {
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    Leaves = db.Attendances.Where(a => a.AttendanceID == AttendanceID).ToList();
                    return Leaves;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public List<Attendance> getAttendanceByEmp(int EmployeeID, int WeekID)
        {
            var Leaves = new List<Attendance>();
            using (var db = new DBModel())
            {
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    Leaves = db.Attendances.Where(a => a.EmployeeID == EmployeeID & a.WeekID == WeekID).ToList();
                    return Leaves;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public int upsertAttendance(Attendance Attendance)
        {
            using (var db = new DBModel())
            {
                if (Attendance.AttendanceID > 0)
                {
                    Attendance l = db.Attendances.Single(a => a.AttendanceID == Attendance.AttendanceID);
                    l.Date = Attendance.Date;
                    l.EmployeeID = Attendance.EmployeeID;
                    l.AttendedHours = Attendance.AttendedHours;
                    l.WeekID = Attendance.WeekID;
                }
                else
                {
                    if (Attendance == null) return 0;
                    db.Attendances.Add(Attendance);
                }
                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    throw e;
                }
                return Attendance.AttendanceID;
            }
        }

        public List<AttendanceXREF> getAttendanceXref(int AttendanceXREFID)
        {
            var Leaves = new List<AttendanceXREF>();
            using (var db = new DBModel())
            {
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    Leaves = db.AttendanceXREFs.Where(a => a.AttendanceXREFID == AttendanceXREFID).ToList();
                    return Leaves;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public List<AttendanceXREF> getAttendanceXrefs(int AttendanceXREFID, int WeekID)
        {
            var Leaves = new List<AttendanceXREF>();
            using (var db = new DBModel())
            {
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    Leaves = db.AttendanceXREFs.Where(a => a.AttendanceXREFID == AttendanceXREFID & a.WeekID == WeekID).ToList();
                    return Leaves;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public List<AttendanceXREF> getAttendanceXrefByS(int EmployeeID, int AttendanceStatusID)
        {
            var Leaves = new List<AttendanceXREF>();
            using (var db = new DBModel())
            {
                try
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    Leaves = db.AttendanceXREFs.Where(a => a.EmployeeID == EmployeeID & a.AttendanceStatusID == AttendanceStatusID).ToList();
                    return Leaves;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public int upsertAttendanceXref(AttendanceXREF AttendanceXREF)
        {
            using (var db = new DBModel())
            {
                if (AttendanceXREF.AttendanceXREFID > 0)
                {
                    AttendanceXREF l = db.AttendanceXREFs.Single(a => a.AttendanceXREFID == AttendanceXREF.AttendanceXREFID);
                    l.EmployeeID = AttendanceXREF.EmployeeID;
                    l.WeekID = AttendanceXREF.WeekID;
                    l.AttendanceStatusID = AttendanceXREF.AttendanceStatusID;
                    l.Comments = AttendanceXREF.Comments;
                }
                else
                {
                    if (AttendanceXREF == null) return 0;
                    //    employee.CreateDate = DateTime.Now;
                    db.AttendanceXREFs.Add(AttendanceXREF);
                }
                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    throw e;
                }
                return AttendanceXREF.AttendanceXREFID;
            }
        }
        //public List<LeaveType> getType()
        //Reason{
        //    var leavetype = new List<LeaveType>();
        //    using (var db = new DBModel())
        //    {
        //        try
        //        {
        //            db.Configuration.LazyLoadingEnabled = false;
        //            leavetype = db.LeaveTypes.ToList();
        //        }
        //        catch (Exception e)
        //        {
        //            throw e;
        //        }
        //    }

        //    return leavetype;
        //}
    }

}